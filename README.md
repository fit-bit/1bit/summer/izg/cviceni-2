IZG Cvičení 2
--------------
Body: 3/3

#### Ovládání aplikace
##### Stisknutí levého tlačítka myši
Určí počáteční bod úsečky

##### Uvolnění levého tlačítka myši
Určí koncový bod úsečky

##### Stisknutí pravého tlačítka myši
Určí střed kružnice

##### Uvolnění pravého tlačítka myši
Určí poloměr kružnice

##### Stisknutí klávesy "D"
Testovací vykreslení obrázku pomocí sady úseček